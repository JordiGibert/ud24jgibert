package com.example.demo.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="empleado")
public class Empleado {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String nombre;
	private String apellido;
	private String puesto;
	private double salario;
	
	//Constructores-----------------------
	public Empleado() {
		
	}

	public Empleado(long id, String nombre, String apellido, String puesto, double salario) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.puesto = puesto;
		this.salario = comprovarPuesto(puesto, salario);
	}
	
//metode comprovar lloc
	private Double comprovarPuesto(String puesto, double salario) {
		switch(puesto) {
			case "camarero":
				salario=1000.0;
				break;
			case "cocinero":
				salario=1500.0;
				break;
			case "encargado":
				salario=1800.0;
				break;
		}
		return salario;
	}
//geters i seters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
		this.salario=comprovarPuesto(puesto, salario);
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
	
	
	
}
