
DROP table IF EXISTS empleado;

create table empleado(
	id int auto_increment,
	nombre varchar(250),
	apellido varchar(250),
	puesto varchar(250),
	salario double
);

insert into empleado (nombre, apellido, puesto, salario)values('Jose','Marin','camarero',1000.0);
insert into empleado (nombre, apellido, puesto, salario)values('Juan','Lopez','encargado',1100.0);
insert into empleado (nombre, apellido, puesto, salario)values('Pedro','Guillem','camarero', 1200.0);
insert into empleado (nombre, apellido, puesto, salario)values('Jordi','Martin','cocinero', 1240.0);
insert into empleado (nombre, apellido, puesto, salario)values('Jonatan','Vicente','cocinero',1200.0);