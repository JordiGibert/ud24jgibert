package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Empleado;

public interface IEmpleadoService {
	
	//CRUD
	public List<Empleado> listarEmpleado();
	
	public Empleado guardarEmpleado(Empleado empleado); //Create
	
	public Empleado empleadoXID(Long id); //READ
	
	public List<Empleado> empleadoXPuesto(String puesto); //READ puesto
	
	public Empleado actualizarEmpleado(Empleado empelado); //UPDATE
	
	public void eliminarEmpleado(Long id); //DELETE :(
	

}
