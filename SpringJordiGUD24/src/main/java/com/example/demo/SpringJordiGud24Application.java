package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJordiGud24Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringJordiGud24Application.class, args);
	}

}
